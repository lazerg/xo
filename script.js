var area = document.getElementById('area');
var move = 0;
var gameIsGoing = true;

var res = false;
var boxes = document.getElementsByClassName('box');

var arr = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],

    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],

    [0, 4, 8],
    [2, 4, 6],
];

area.addEventListener('click', function (event) {

    if (event.target.innerHTML === "" && gameIsGoing) {
        event.target.innerHTML = move % 2 === 0 ? Math.random() < 0.5 ? 'X' : 'x' : Math.random() < 0.5 ? 'O' : 'o';
        move++;

        for (var i = 0; i < 8; i++) {
            if ((boxes[arr[i][0]].innerHTML === "X" || boxes[arr[i][0]].innerHTML === "x") &&
                (boxes[arr[i][1]].innerHTML === "X" || boxes[arr[i][1]].innerHTML === "x") &&
                (boxes[arr[i][2]].innerHTML === "X" || boxes[arr[i][2]].innerHTML === "x")) {
                res = 1;
            } else if ((boxes[arr[i][0]].innerHTML === "O" || boxes[arr[i][0]].innerHTML === "o") &&
                (boxes[arr[i][1]].innerHTML === "O" || boxes[arr[i][1]].innerHTML === "o") &&
                (boxes[arr[i][2]].innerHTML === "O" || boxes[arr[i][2]].innerHTML === "o")) {
                res = 2;
            }
        }
        if (res !== false) {
            document.getElementsByClassName('result')[0].innerHTML = "Congratulations team of " + (res ? 'X' : 'O') + "<br><input onclick='location.reload()' type='button' value='Restart'>";
            gameIsGoing = false;
        } else if (move == 9) {
            document.getElementsByClassName('result')[0].innerHTML = "DRAW!<br><input onclick='location.reload()' type='button' value='Restart'>";
        }

    }
});